<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Tag extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tag')->insert([
            [
            'name' => 'Tag 1',
            ],
            [
                'name' => 'Tag 2',
            ],
            [
                'name' => 'Tag 3',
            ],
            [
                'name' => 'Tag 4',
            ],
            [
                'name' => 'Tag 5',
            ]
        ]);
    }
}
