<?php
namespace App;

use Illuminate\Database\Eloquent\Model;


class Pet extends Model
{
  
    protected $fillable = ['id','name'];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'pet';

    /**
     * @return mixed
     */
    public function category()
    {
      return $this->belongsToMany('App\Category', 'pet_category', 'id', 'category_id');
    }

}