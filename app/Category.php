<?php
namespace App;

use Illuminate\Database\Eloquent\Model;


class Category extends Model
{
  
    protected $fillable = ['id','name'];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'category';



    public function pet()
    {
      return $this->belongsToMany('App\Pet', 'pet_category', 'pet_id', 'category_id');
    }

}