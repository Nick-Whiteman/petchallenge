<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Pet;
use App\Category;

class PetCategory extends Model
{
  
    protected $fillable = ['id','pet_id','category_id'];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'pet_category';


    /**
     * @return mixed
     */
    public function category()
    {
        return $this->hasOne('App\Category', 'category_id','id');
    }

        /**
     * @return mixed
     */
    public function pet()
    {
        return $this->hasOne('App\Pet', 'pet_id','id');
    }

}