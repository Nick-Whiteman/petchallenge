<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Pet;

class PetController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        //
    }

    public function show($id)
    {
        $pet = Pet::Find($id)->with('Category')->firstOrFail();
        return response()->json(array_merge($pet->only('id', 'name'), [
            'category' => $pet->category->map(function ($category) {
                return $category->only(['id', 'name']);
            })->toArray()
        ]));
    }
}
