<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Pet extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pet')->insert([
            [
            'name' => 'Dog',
            ],
            [
                'name' => 'Cat',
            ],
            [
                'name' => 'Bird',
            ],
            [
                'name' => 'Mouse',
            ],
            [
                'name' => 'Fish',
            ]
        ]);
    }
}
