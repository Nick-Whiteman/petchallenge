<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;



class Category extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('category')->insert([
            [
            'name' => 'Fluffy',
            ],
            [
                'name' => 'InDoor',
            ],
            [
                'name' => 'Outdoor',
            ],
            [
                'name' => 'Aquatic',
            ],
            [
                'name' => 'Caged',
            ]
        ]);
    }
}
