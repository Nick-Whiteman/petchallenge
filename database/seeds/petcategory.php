<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PetCategory extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pet_category')->insert([
            [
                'pet_id' => '3',
                'category_id' => '3',
            ],
            [
                'pet_id' => '3',
                'category_id' => '2',
            ],
        ]);
    }
}
